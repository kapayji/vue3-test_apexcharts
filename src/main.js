import { createApp } from "vue";
import VueApexCharts from "vue3-apexcharts";
import store from "./store/index.js";
import App from "./App.vue";

const app = createApp(App);
app.use(VueApexCharts);
app.use(store);
app.mount("#app");
