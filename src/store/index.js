import { createStore } from "vuex";

const store = createStore({
  state: {
    categories: [],
    series: [{ name: "", data: [] }],
  },
  mutations: {
    addToCategories(state, categorie) {
      state.categories = [...state.categories, categorie];
    },
    addToSeries(state, { name, data }) {
      state.series.forEach((el) =>
        el === name
          ? el.data.push(data)
          : (state.series = [...state.series, [{ name, data }]])
      );
    },
  },
  actions: {
    addToCategories({ commit }, categorie) {
      commit("addToCategories", categorie);
    },
    addToSeries({ commit }, { name, data }) {
      commit("addToSeries", { name, data });
    },
  },
  getters: {
    getCategories(state) {
      return state.categories;
    },
    getSeries(state) {
      return state.series;
    },
  },
});
export default store;
